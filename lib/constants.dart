import 'package:flutter/material.dart';

BoxDecoration kCustomDecorationBack = new  BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
    colors: [
      Color.fromRGBO(25, 27, 58, 1),
      Color.fromRGBO(47, 67, 113, 1),
    ],
  ),
);

