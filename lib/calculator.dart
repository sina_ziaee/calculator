import 'package:stack/stack.dart';

class Calculator{

  List<String> operators = ['+','-','*','/','%'];

  List<String> numbers = ['0','1','2','3','4','5','6','7','8','9'];

  String calculate(String expression){
    String result = evaluate(expression).toString();
//    print(result);
    return result;
  }

  double evaluate(String expression)
  {
    List<String> tokens = [];

    for(int i=0;i<expression.length;i++){
      tokens.add(expression[i]);
    }

//    print(tokens);

    // Stack for numbers: 'values'
    Stack<double> values = new Stack<double>();

    // Stack for Operators: 'ops'
    Stack<String> ops = new Stack<String>();

    for (int i = 0; i < tokens.length; i++)
    {
      // Current token is a whitespace, skip it
      if (tokens[i] == ' '){
        continue;
      }

      // Current token is a number, push it to stack for numbers
      if (numbers.contains(tokens[i]))
      {
        String str = "";
        // There may be more than one digits in number
        while (i < tokens.length && numbers.contains(tokens[i])){
          str += tokens[i++];
        }
        i--;
//        print(str);
        values.push(double.parse(str));
      }

      // Current token is an opening brace, push it to 'ops'
      else if (tokens[i] == '('){
        ops.push(tokens[i]);
      }

      // Closing brace encountered, solve entire brace
      else if (tokens[i] == ')')
      {
        while (ops.top() != '('){
          values.push(applyOp(ops.pop(), values.pop(), values.pop()));
        }
        ops.pop();
      }

      // Current token is an operator.
      else if (tokens[i] == '+' || tokens[i] == '-' ||
          tokens[i] == '*' || tokens[i] == '/')
      {
        // While top of 'ops' has same or greater precedence to current
        // token, which is an operator. Apply operator on top of 'ops'
        // to top two elements in values stack
        while (ops.isNotEmpty && hasPrecedence(tokens[i], ops.top())) {
//          print('here2');
          values.push(applyOp(ops.pop(), values.pop(), values.pop()));
        }

        // Push current token to 'ops'.
        ops.push(tokens[i]);
      }
    }

    // Entire expression has been parsed at this point, apply remaining
    // ops to remaining values
    while (ops.isNotEmpty){
//      print('here1');
      values.push(applyOp(ops.pop(), values.pop(), values.pop()));
    }

    // Top of 'values' contains result, return it
    double result = values.pop();
//    print(values.top());
//    print(ops.top());
//    print(result);
    return result;
  }

  bool hasPrecedence(String op1, String op2)
  {
    if (op2 == '(' || op2 == ')')
      return false;
    if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
      return false;
    else
      return true;
  }

  double applyOp(String op, double b, double a)
  {
    switch (op)
    {
      case '+':
        print(a+b);
        return a + b;
      case '-':
        print(a-b);
        return a - b;
      case '*':
        print(a*b);
        return a * b;
      case '/':
        print(a/b);
        if (b == 0)
          throw ("Cannot divide by zero");
        return a / b;
    }
    return 0;
  }

}