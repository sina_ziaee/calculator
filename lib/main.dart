import 'package:calculator/calculator.dart';
import 'package:calculator/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';

Calculator calculator = new Calculator();

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CalculatorBody(),
    ),
  );
}

class CalculatorBody extends StatefulWidget {
  @override
  _CalculatorBodyState createState() => _CalculatorBodyState();
}

class _CalculatorBodyState extends State<CalculatorBody> {
  String line = '';
  String result = '';
  bool isVisible = false;

  int countL = 0;
  int countR = 0;

  void update() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Container(
      decoration: kCustomDecorationBack,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          children: [
            Flexible(
              flex: 4,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      flex: 2,
                      child: Container(),
                    ),
                    Flexible(
                      flex: 1,
                      child: Text(
                        line,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(),
                    ),
                    Flexible(
                      flex: 1,
                      child: Visibility(
                        visible: isVisible,
                        child: Text(
                          result,
                          style: TextStyle(color: Colors.white, fontSize: 35),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Container(),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),),
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 70),
                      color: Color.fromRGBO(28, 35, 69, 1),
                      onPressed: () {
                        setState(
                          () {
                            setState(() {
                              operatorPressed('(');
                              countL++;
                            });
                          },
                        );
                      },
                      child: Text(
                        '(',
                        style: TextStyle(fontSize: 25, color: Colors.white),
                      ),
                    ),
                  ),
                  Flexible(
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 70),
                      color: Color.fromRGBO(28, 35, 69, 1),
                      onPressed: () {
                        setState(() {
                          parantesizesPressed(')');
                          countR++;
                        });
                      },
                      child: Text(
                        ')',
                        style: TextStyle(fontSize: 25, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 5,
              child: Row(
                children: [
                  Flexible(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomNumberButton(
                              number: '7',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('7');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '8',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('8');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '9',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('9');
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomNumberButton(
                              number: '4',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('4');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '5',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('5');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '6',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('6');
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomNumberButton(
                              number: '1',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('1');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '2',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('2');
                                  },
                                );
                              },
                            ),
                            CustomNumberButton(
                              number: '3',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('3');
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomNumberButton(
                              number: '0',
                              onPressed: () {
                                setState(
                                  () {
                                    numberPressed('0');
                                  },
                                );
                              },
                            ),
                            Flexible(
                              flex: 1,
                              child: Container(
                                height: 56,
                                width: 56,
                              ),
                            ),
                            CustomNumberButton(
                              number: 'C',
                              onPressed: () {
                                setState(
                                  () {
                                    deleteAll();
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Flexible(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              CustomOperandButton(
                                number: '-',
                                onPressed: () {
                                  setState(
                                    () {
                                      operatorPressed('-');
                                    },
                                  );
                                },
                              ),
                              CustomRemoveButton(
                                icon: Icons.backspace,
                                onPressed: () {
                                  setState(
                                    () {
                                      removeOneChar();
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Flexible(
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30),
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        Colors.orangeAccent[100],
                                        Colors.orange
                                      ],
                                    ),
                                  ),
                                  width: 55,
                                  height: 100,
                                  child: FlatButton(
                                    highlightColor: Colors.orangeAccent[100],
                                    onPressed: () {
                                      setState(
                                        () {
                                          operatorPressed('+');
                                        },
                                      );
                                    },
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 30),
                                    ),
                                  ),
                                ),
                              ),
                              Flexible(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      child: CustomOperandButton(
                                        number: '*',
                                        onPressed: () {
                                          setState(
                                            () {
                                              operatorPressed('*');
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                    Flexible(
                                      child: CustomOperandButton(
                                        number: '/',
                                        onPressed: () {
                                          setState(() {
                                            operatorPressed('/');
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Flexible(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              gradient: LinearGradient(
                                colors: [Colors.pink[300], Colors.pink[500]],
                              ),
                            ),
                            width: 130,
                            height: 55,
                            child: FlatButton(
                              highlightColor: Colors.pink[300],
                              onPressed: () {
                                setState(() {
                                  doCalculations();
                                });
                              },
                              child: Text(
                                '=',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 30),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void removeOneChar(){
    if(line.length != 0){
      line = line.substring(0, line.length-1);
    }
  }

  void doCalculations(){
    if(line.length != 0){
      isVisible = true;
      if(line.contains('/0')){
        result = 'Cannot divide by zero';
        deleteAll();
      }
      else if(countL != countR){
        result = 'Bad usage of ( and )';
        deleteAll();
      }
      else {
        String result = calculator.calculate(line);
        print(result);
        this.result = result;
      }
    }
  }

  void deleteAll() {
    line = '';
  }

  void numberPressed(String number) {
    line += number;
    isVisible = false;
  }

  void parantesizesPressed(String op){
    line += op;
  }

  void operatorPressed(String operator) {
    isVisible = false;
    if(line.length == 0){
      // pass
    }
    else if (calculator.operators
        .contains(line.substring(line.length - 1, line.length))) {
      // pass
    } else {
      line += operator;
    }
  }
}

class CustomNumberButton extends StatelessWidget {
  final String number;
  final Function onPressed;

  CustomNumberButton({@required this.number, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 1,
      child: RawMaterialButton(
        shape: CircleBorder(),
        constraints: BoxConstraints.tightFor(
          height: 56,
          width: 56,
        ),
        child: Text(
          number,
          style: TextStyle(fontSize: 25, color: Colors.white),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class CustomOperandButton extends StatelessWidget {
  final String number;
  final Function onPressed;

  CustomOperandButton({@required this.number, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      width: 56,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.yellow[400], Colors.orange[900]],
        ),
      ),
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        splashColor: Colors.yellow[400],
        highlightColor: Colors.yellow[400],
        child: Center(
          child: Text(
            number,
            style: TextStyle(fontSize: 25, color: Colors.white),
          ),
        ),
        onTap: onPressed,
      ),
    );
  }
}

class CustomRemoveButton extends StatelessWidget {
  final IconData icon;
  final Function onPressed;

  CustomRemoveButton({@required this.icon, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      width: 56,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.yellow[400], Colors.orange[900]],
        ),
      ),
      child: InkWell(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        splashColor: Colors.yellow[400],
        highlightColor: Colors.yellow[400],
        child: Center(
          child: Icon(
            icon,
            color: Colors.white,
            size: 22,
          ),
        ),
        onTap: onPressed,
      ),
    );
  }
}